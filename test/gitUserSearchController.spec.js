describe('GitUserSearchController', function() {
  beforeEach(module('GitUserSearch'));
  var ctrl, fakeSearch;

  beforeEach(function() {
    fakeSearch = jasmine.createSpyObj('fakeSearch', ['query']);
    module({
      Search: fakeSearch
    });
  });

  beforeEach(inject(function($controller) {
    ctrl = $controller('GitUserSearchController');
  }));

  it('initialises with an empty search result and term', function() {
    expect(ctrl.searchResult).toBeUndefined();
    expect(ctrl.searchTerm).toBeUndefined();
  });

  describe('when searching for a user', function() {
    var scope;
    var gitHubSearchResponse = {
      "items" : [
        {
          "login"     : "tansaku",
          "avatar_url": "https://avatars.githubusercontent.com/u/30216?v=3",
          "html_url"  : "https://github.com/tansaku"
        }
      ]
    };

    beforeEach(inject(function($q, $rootScope) {
      scope = $rootScope;
      fakeSearch.query.and.returnValue($q.when( { data: gitHubSearchResponse }));
    }));

    it('displays search results', function() {
      ctrl.searchTerm = 'tansaku';
      ctrl.doSearch();
      scope.$apply();
      expect(ctrl.searchResult).toEqual(gitHubSearchResponse);
    });
  });
});
